* Boolean cVimrc settings are enabled with the command 'set'+`<SETTING_NAME>` and disabled with the 'set'+`no<SETTING_NAME>` (for example,set regexp and set noregexp)
* Boolean cVimrc settings can be inversed by adding "!" to the end 
* Other settings are defined with `=` used as a separator and are prefixed by let (for example, let hintcharacters="abc")
searchlimit(命令条显示的结果总数)
scrollstop(设置总像素滚动当用向上滚动和向下滚动命令时)
timeoutlen(等待<Leader>命令的总时间，用毫秒计)
fullpagescrollpercent(当用向上全页滚动和向下全页滚动的页面百分比)
typelinkhintsdelay()
scrollduration
vimport(设置端口用于editWithVim insert mode command)
zoomfactor(扩大与缩小页面步长)
scalehints(当他们出现时动画链接)
hud(展示the heads-up-display)
regexp(在查找模式中用与正则匹配)
ignorecase(忽略大小写)
linkanimations(展示消失效果当链接开和关时)
numerichints(use numbers for link hints instead of a set of characters)
dimhintcharacters(dim letter matches in hint characters rather than remove  them from the hint)
defaultnewtabpage(默认的chrome://newtab page instead of a blank page)
cncpcompletion(用C-n和C-p去遍历补全结果)
smartcase(case-insesitive find mode 搜索)
incsearch(当输入长度超过两个字符时匹配高亮)
typelinkhints()
autohidecursor()
autofouces
    insertmapping
    smoothscroll
    autoupdategist
    natvielinkorder
    showtabindices
    sortlinkhints
    localconfig
    completeonopen
    configpath
    changelog
    completionengines
    blacklists
    mapleader
    defaultengine
    locale
    homedirectory
    qmark<alphanumericcharacter>
    previousmatchpattern
    nextmatchpattern
    hintcharacters
    barposition
    langmap

