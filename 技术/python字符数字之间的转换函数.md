int (x[,base])  将x转换为一个整数
long(x [,base]) 将x转换为一个长整数
float(x)    将x转换为一个浮点数
complex(real [,imag])   创建一个复数
str(x)  将对象x转换为字符串
repr(x) 将对象x转换为表达式字符串
eval(str)   用来计算在字符串中的有效python表达式，并返回一个对象
tuple(s)    将序列s转换为一个元组
list(s)     将序列s转换为一个列表
chr(x)  将一个整数转换为一个字符
unichr(x)   将一个整数转换为unicode字符
ord(x)  将一个字符转换为它的整数值
hex(x)  将一个整数转换为一个十六进制字符串
oct(x)  将一个整数转换为一个八进制字符串
